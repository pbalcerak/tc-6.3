const express = require('express')
const mongodb = require('mongodb')
const cors = require('cors')

const app = express()
app.use(cors())

const port = 4000

const mongoHost = 'host.docker.internal'
const mongoUrl = `mongodb://${mongoHost}:27017/test`

// const client = new mongodb.MongoClient(mongoUrl, {
//     // useNewUrlParser: true,
//     // useUnifiedTopology: true
// })

const client = new mongodb.MongoClient(mongoUrl)

app.get('/', async (req, res) => {
    try {
        const db = client.db('test')
        const collection = db.collection('restaurants')
        const data = await collection.find({}).limit(5).toArray()
        res.json(data)
    } catch (err) {
        console.error(err)
        res.status(500).send('Error')
    }
})

app.listen(port, () => {
    console.log(`App listening on port ${port}`)
})