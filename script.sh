#!/bin/bash
docker build -t my_frontend -f ./frontend/Dockerfile ./frontend
docker build -t my_backend -f ./backend/Dockerfile ./backend

docker run -d --network backend_network --name mongo -v=data-vol:/data/db -p 27017:27017 mongo
docker run -d --network backend_network --name backend -p 4000:4000 my_backend
docker network connect frontend_network backend
docker run -d --network frontend_network --name frontend -p 3000:3000 my_frontend


